$(document).change(function(event) {
    if (event.target.id === "sCategory" || event.target.id === "sCategoryE") {
        var url;
        if (event.target.id === "sCategory") {
            url = "../subcategories/" + event.target.value;
        } else {
            url = "../../subcategories/" + event.target.value;
        }
        $.get(url,
            function(response, state) {
                var select = document.getElementById("sSubcategory");
                var length = select.options.length;
                for (i = length - 1; i >= 0; i--) {
                    select.options[i] = null;
                }

                let aSubcategories = response.data;
                for (let i = 0; i < aSubcategories.length; i++) {
                    const aSubcategory = aSubcategories[i];
                    var option = document.createElement("option");
                    option.text = aSubcategory[1];
                    option.value = aSubcategory[0];
                    document.getElementById("sSubcategory").appendChild(option);
                }
            });
    }
});
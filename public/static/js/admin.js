var base = location.protocol + '//' + location.host;

$(document).ready(function() {
    editor_init('editor');
})

function editor_init(field) {
    //CKEDITOR.plugins.ddExternal('codesnippet', base + '/static/libs/ckeditor/plugins/codesnippet/', 'plugins.js');
    CKEDITOR.replace(field, {
        //skin: 'mono',
        //extraPlugins: 'codesnippet, ajax,xml,textmatch,autocomplete,textwatcher,emoji,panelutton,preview,wordcount',
        toolbar: [
            { name: 'clipboard', 'items': ['Cut', 'Copy', 'Paste', 'Pastetext', '-', 'Undo', 'Redo'] },
            { name: 'basicstyles', 'items': ['Bold', 'Italic', 'BulletedList', 'Strike', 'Image', 'Link', 'Unlink', 'Blockquote'] },
            { name: 'document', 'items': ['CodeSnippet', 'EmojiPanel', 'Preview', 'Source'] },
        ]
    });
}
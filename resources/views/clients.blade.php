<section class="clients_logo_area section-padding">
    <div class="container">
        <div class="clients_slider owl-carousel">
            <div class="item">
                <img src="{{ url('/page/img/clients-logo/auteco.png') }}" alt="">
            </div>
            <div class="item">
                <img src="{{ url('/page/img/clients-logo/superInter.png') }}" alt="">
            </div>
            <div class="item">
                <img src="{{ url('/page/img/clients-logo/exito.png') }}" alt="">
            </div>
            <div class="item">
                <img src="{{ url('/page/img/clients-logo/ciac.png') }}" alt="">
            </div>
            <div class="item">
                <img src="{{ url('/page/img/clients-logo/asoflovi.png') }}" alt="">
            </div>
        </div>
    </div>
</section>

<section class="hero-banner mb-30px">
         <div class="container">
             <div class="row">
                 <div class="col-lg-7">
                     <div class="hero-banner__img">
                         <img class="img-fluid" src="{{ url('/page/img/banner/banner.png') }}" alt="">
                     </div>
                 </div>
                 <div class="col-lg-5 pt-5">
                     <div class="hero-banner__content">
                         <h1>@lang('i18n.banner-title')</h1>
                         <h3>@lang('i18n.banner-text')</h3>
                         <br>
                         <br>
                         <br>
                         <br>
                         <br>
                         <br>
                         <br>
                         <br>
                         {{-- <a class="button bg" href="#">Get Started</a> --}}
                     </div>
                 </div>
             </div>
         </div>
     </section>
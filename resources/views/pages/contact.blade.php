@extends('master')

@section('title', 'Contacto')

@section('content')

<!-- ================ start footer Area ================= -->
@include('header')
<!-- ================ End footer Area ================= -->


<!--================ Hero sm Banner start =================-->
<section class="hero-banner hero-banner--sm mb-30px">
    <div class="container">
        <div class="hero-banner--sm__content">
            <h1>@lang('i18n.contact')</h1>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">@lang('i18n.home')</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@lang('i18n.contact')</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<!--================ Hero sm Banner end =================-->


<!-- ================ contact section start ================= -->
<section class="section-margin">
    <div class="container">


        @include('persons')
        <br>
        <br>
        <br>
        <br>

        <div class="row">
            <div class="col-md-12 col-lg-12">
                <form action="#/" class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <input class="form-control" name="name" id="name" type="text" placeholder="@lang('i18n.enter') @lang('i18n.name')" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="email" id="email" type="email" placeholder="@lang('i18n.enter') @lang('i18n.email')" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="subject" id="subject" type="text" placeholder="@lang('i18n.enter') @lang('i18n.subject')" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="form-group">
                                <textarea class="form-control different-control w-100" name="message" id="message" cols="30" rows="5" placeholder="@lang('i18n.enter') @lang('i18n.message')" autocomplete="off"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center text-md-right mt-3">
                        <button type="submit" class="button button-contactForm">@lang('i18n.send') @lang('i18n.message')</button>
                    </div>
                </form>
            </div>


        </div>

    </div>
</section>
<!-- ================ contact section end ================= -->


<!-- ================ start footer Area ================= -->
@include('footer')
<!-- ================ End footer Area ================= -->


@endsection

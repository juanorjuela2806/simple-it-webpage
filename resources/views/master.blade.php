<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Simple-IT | Keep It Simple.</title>
    <link rel="icon" href="{{ url('/page/img/favicon.png') }}" type="image/png">

    <link rel="stylesheet" href="{{ url('/page/vendors/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/page/vendors/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ url('/page/vendors/themify-icons/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ url('/page/vendors/linericon/style.css') }}">
    <link rel="stylesheet" href="{{ url('/page/vendors/owl-carousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ url('/page/vendors/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ url('/page/css/style.css') }}">

</head>
<body>

    @section('content')
    @show

    <script src="{{ url('/page/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ url('/page/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('/page/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ url('/page/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ url('/page/js/mail-script.js') }}"></script>
    <script src="{{ url('/page/js/main.js') }}"></script>

</body>

</html>

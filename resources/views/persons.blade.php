 <div class="row">
     <div class="col-md-3 col-6">
         <div class="media contact-info">
             <span class="contact-info__icon">
                 <img src="{{ url('/page/img/colombia.png') }}" alt="" style="width:2em;">
                 <br><br>
                 <h3><strong>@lang('i18n.person1')</strong></h3>
                 <p>@lang('i18n.phonePerson1')</p>
                 <p>@lang('i18n.cityPerson1')</p>
                 <a href="https://www.linkedin.com/in/andres-felipe-garcia-ortiz/"><i class="fab fa-linkedin-in"></i></a>
             </span>
         </div>
     </div>
     <div class="col-md-3 col-6">
         <div class="media contact-info">
             <span class="contact-info__icon">
                 <img src="{{ url('/page/img/colombia.png') }}" alt="" style="width:2em;">
                 <br><br>
                 <h3><strong>@lang('i18n.person2')</strong></h3>
                 <p>@lang('i18n.phonePerson2')</p>
                 <p>@lang('i18n.cityPerson2')</p>
                 <a href="https://www.linkedin.com/in/cavillegastabares"><i class="fab fa-linkedin-in"></i></a>
             </span>
         </div>
     </div>     
     <div class="col-md-3 col-6">
         <div class="media contact-info">
             <span class="contact-info__icon">
                 <img src="{{ url('/page/img/colombia.png') }}" alt="" style="width:2em;">
                 <br><br>
                 <h3><strong>@lang('i18n.person3')</strong></h3>
                 <p>@lang('i18n.phonePerson3')</p>
                 <p>@lang('i18n.cityPerson3')</p>
                 <a href="https://www.linkedin.com/in/juanorjuela/"><i class="fab fa-linkedin-in"></i></a>
             </span>
         </div>
     </div>
     <div class="col-md-3 col-6">
         <div class="media contact-info">
             <span class="contact-info__icon">
                 <img src="{{ url('/page/img/spain.png') }}" alt="" style="width:2em;">
                 <br><br>
                 <h3><strong>@lang('i18n.person4')</strong></h3>
                 <p>@lang('i18n.phonePerson4')</p>
                 <p>@lang('i18n.cityPerson4')</p>
                 <a href="https://www.linkedin.com/in/sebastiandiaz-dev/"><i class="fab fa-linkedin-in"></i></a>
             </span>
         </div>
     </div>
 </div>

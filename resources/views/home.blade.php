 <main class="side-main">
     <!--================ Hero sm Banner start =================-->
     @include('banner')
     <!--================ Hero sm Banner end =================-->

     <!--================ Feature section start =================-->
     @include('features')
     <!--================ Feature section end =================-->

     <!--================ about section start =================-->
     @include('section-one')
     <!--================ about section end =================-->

     <!--================ Offer section start =================-->
    @include('offers')
     <!--================ Offer section end =================-->

     <!--================ Solution section start =================-->
     @include('solution')
     <!--================ Solution section end =================-->

     <!--================ Pricing section start =================-->
     {{-- @include('solution') --}}
     <!--================ Pricing section end =================-->

     <!--================ Testimonial section start =================-->
     {{-- @include('testimonial') --}}
     <!--================ Testimonial section end =================-->
    
 </main>

<section class="section-margin">
     <div class="container">
         <div class="section-intro pb-85px text-center">
             <h2 class="section-intro__title">@lang('i18n.offers-title')</h2>
             <p class="section-intro__subtitle">@lang('i18n.offers-subtitle')</p>
         </div>

         <div class="row">
             <div class="col-lg-6">

                 <div class="row offer-single-wrapper">
                     <div class="col-lg-4 col-6 offer-single">
                         <div class="card offer-single__content text-center">
                             <span class="offer-single__icon">
                                 <img class="img-fluid" src="{{ url('/page/img/code/laravel.png') }}" alt="" style="width: 5em;">
                             </span>
                             <h4>Laravel</h4>
                             <p></p>
                         </div>
                     </div>
                     <div class="col-lg-4 col-6 offer-single">
                         <div class="card offer-single__content text-center">
                             <span class="offer-single__icon">
                                 <img class="img-fluid" src="{{ url('/page/img/code/react.png') }}" alt="" style="width: 3em;">
                             </span>
                             <h4>React</h4>
                             <p></p>
                         </div>
                     </div>
                     <div class="col-lg-4 col-6 offer-single">
                         <div class="card offer-single__content text-center">
                             <span class="offer-single__icon">
                                 <img class="img-fluid" src="{{ url('/page/img/code/ionic.png') }}" alt="" style="width: 3em;">
                             </span>
                             <h4>Ionic</h4>
                             <p></p>
                         </div>
                     </div>
                 {{-- </div>

                 <div class="row offer-single-wrapper"> --}}
                     <div class="col-lg-4 col-6  offer-single">
                         <div class="card offer-single__content text-center">
                             <span class="offer-single__icon">
                                 <img class="img-fluid" src="{{ url('/page/img/code/js.png') }}" alt="" style="width: 3em;">
                             </span>
                             <h4>JavaScript</h4>
                             <p></p>
                         </div>
                     </div>
                     <div class="col-lg-4 col-6  offer-single">
                         <div class="card offer-single__content text-center">
                             <span class="offer-single__icon">
                                 <img class="img-fluid" src="{{ url('/page/img/code/node.png') }}" alt="" style="width: 3.5em;">
                             </span>
                             <h4>NodeJS</h4>
                             <p></p>
                         </div>
                     </div>
                     <div class="col-lg-4 col-6  offer-single">
                         <div class="card offer-single__content text-center">
                             <span class="offer-single__icon">
                                 <img class="img-fluid" src="{{ url('/page/img/code/angular.png') }}" alt="" style="width: 3em;">
                             </span>
                             <h4>Angular</h4>
                             <p></p>
                         </div>
                     </div>
                 </div>

                 <div class="row offer-single-wrapper">

                     <div class="col-lg-12 offer-single">
                         <div class="card offer-single__content text-center">
                             <span class="offer-single__icon">
                                 <img class="img-fluid" src="{{ url('/page/img/code/aws_logo.png') }}" alt="" style="width: 3em;">
                             </span>
                             <h4>Amazon Web Services - Hosting y Arquitectura</h4>
                             <p></p>
                         </div>
                     </div>

                 </div>
                 <div class="row offer-single-wrapper">

                     <div class="col-lg-12 offer-single">
                         <div class="card offer-single__content text-center">
                             <span class="offer-single__icon">
                                 <img class="img-fluid" src="{{ url('/page/img/code/SAPCP.png') }}" alt="" style="width: 15em;">
                                 <br>
                                 <img class="img-fluid" src="{{ url('/page/img/code/UI5.png') }}" alt="" style="width: 3em;">
                             </span>
                             <h4>SAP Cloud Platform - SAP Fiori - SAPUI5</h4>
                             <p></p>
                         </div>
                     </div>

                 </div>

             </div>
             <div class="col-lg-6">
                 <div class="offer-single__img">
                     <img class="img-fluid" src="{{ url('/page/img/home/offer.png') }}" alt="">
                 </div>
             </div>
         </div>
     </div>
 </section>

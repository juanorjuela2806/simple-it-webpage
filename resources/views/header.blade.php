 <header class="header_area">
     <div class="main_menu">
         <nav class="navbar navbar-expand-lg navbar-light">
             <div class="container box_1620">
                 <!-- Brand and toggle get grouped for better mobile display -->
                 <a class="navbar-brand logo_h" href="/"><img src="{{ url('/page/img/logo.png') }}" alt=""></a>
                 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                 </button>

                 <!-- Collect the nav links, forms, and other content for toggling -->
                 <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                     <ul class="nav navbar-nav menu_nav justify-content-end">
                         <li class="nav-item active"><a class="nav-link" href="/">@lang('i18n.home')</a></li>
                         {{-- <li class="nav-item"><a class="nav-link" href="feature.html">Feature</a></li> --}}
                         {{-- <li class="nav-item"><a class="nav-link" href="pricing.html">Price</a> --}}
                         <li class="nav-item"><a class="nav-link" href="/contact">@lang('i18n.contact')</a></li>
                         <li class="nav-item submenu dropdown">
                             <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@lang('i18n.language')</a>
                             <ul class="dropdown-menu">
                                 <li class="nav-item"><a class="nav-link" href="/site/es">@lang('i18n.spanish')</a></li>



                                 <li class="nav-item"><a class="nav-link" href="/site/en">@lang('i18n.english')</a></li>
                             </ul>
                         </li>
                     </ul>

                     <ul class="navbar-right">
                         <li class="nav-item">
                             <!-- <a class="button button-header bg" href="/helpdesk" target="_blank">Helpdesk</a> -->
                         </li>
                     </ul>
                 </div>
             </div>
         </nav>
     </div>
 </header>


<section class="section-padding--small bg-magnolia">
    <div class="container">
        <div class="row no-gutters align-items-center">
            <div class="col-md-5 mb-5 mb-md-0">
                <div class="about__content">
                    <h2>@lang('i18n.section-one-title')</h2>
                    <p>
                        @lang('i18n.section-one-text')
                    </p>
                    {{-- <a class="button button-light" href="#">Know More</a> --}}
                </div>
            </div>
            <div class="col-md-7">
                <div class="about__img">
                    <img class="img-fluid" src="{{ url('/page/img/home/about.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

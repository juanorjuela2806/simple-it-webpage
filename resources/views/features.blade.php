<section class="section-margin">
         <div class="container">
             <div class="section-intro pb-85px text-center">
                 <h2 class="section-intro__title">@lang('i18n.features-title')</h2>
                 <p class="section-intro__subtitle">@lang('i18n.features-text')</p>
             </div>

             <div class="container">
                 <div class="row">
                     <div class="col-lg-4" style="margin-bottom: 2%;">
                         <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
                             <span class="card-feature__icon">
                                 <i class="ti-package"></i>
                             </span>
                             <h3 class="card-feature__title">@lang('i18n.features-title01')</h3>
                             <p class="card-feature__subtitle">@lang('i18n.features-subtitle01')</p>
                         </div>
                     </div>                     
                     <div class="col-lg-4" style="margin-bottom: 2%;">
                         <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
                             <span class="card-feature__icon">
                                 <i class="ti-cloud"></i>
                             </span>
                             <h3 class="card-feature__title">@lang('i18n.features-title02')</h3>
                             <p class="card-feature__subtitle">@lang('i18n.features-subtitle02')</p>
                         </div>
                     </div>
                     <div class="col-lg-4" style="margin-bottom: 2%;">
                         <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
                             <span class="card-feature__icon">
                                 <i class="ti-headphone-alt"></i>
                             </span>
                             <h3 class="card-feature__title">@lang('i18n.features-title03')</h3>
                             <p class="card-feature__subtitle">@lang('i18n.features-subtitle03')</p>
                             <br>                             
                         </div>
                     </div>

                     <div class="col-lg-4">
                         <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
                             <span class="card-feature__icon">
                                 <i class="ti-server"></i>
                             </span>
                             <h3 class="card-feature__title">@lang('i18n.features-title04')</h3>
                             <p class="card-feature__subtitle">@lang('i18n.features-subtitle04')</p>
                         </div>
                     </div>

                     <div class="col-lg-4">
                         <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
                             <span class="card-feature__icon">
                                 <i class="ti-mobile"></i>
                             </span>
                             <h3 class="card-feature__title">@lang('i18n.features-title05')</h3>
                             <p class="card-feature__subtitle">@lang('i18n.features-subtitle05')</p>
                         </div>
                     </div>

                     <div class="col-lg-4">
                         <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
                             <span class="card-feature__icon">
                                 <i class="ti-truck"></i>
                             </span>
                             <h3 class="card-feature__title">@lang('i18n.features-title06')</h3>
                             <p class="card-feature__subtitle">@lang('i18n.features-subtitle06')</p>                             
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>

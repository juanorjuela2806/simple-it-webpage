@extends('master')

@section('title', 'Inicio')

@section('content')

<!-- ================ start footer Area ================= -->
@include('header')
<!-- ================ End footer Area ================= -->



@include('home')


<!-- ================ start footer Area ================= -->
@include('footer')
<!-- ================ End footer Area ================= -->


@endsection

<section class="section-padding--small bg-magnolia">
    <div class="container">
        <div class="row align-items-center pt-xl-3 pb-xl-5">
            <div class="col-lg-6">
                <div class="solution__img text-center text-lg-left mb-4 mb-lg-0">
                    <img class="img-fluid" src="{{ url('/page/img/home/solution.png') }}" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="solution__content">
                    <h2>@lang('i18n.solution-title')</h2>
                    <p>@lang('i18n.solution-text')</p>
                    {{-- <a class="button button-light" href="#">Know More</a> --}}
                </div>
            </div>
        </div>
        @include('clients')
    </div>
</section>

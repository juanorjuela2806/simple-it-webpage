<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

'home' => 'Home',
'contact' => 'Contact us',
'home' => 'Home',

'banner-title' => 'Simple-IT',
'banner-text' => 'Keep It Simple!',
'features-title' => 'Services',
'features-text' => 'Using mobile services, Big Data, machine learning, internet of things and more, to offer secure business apps that integrate and optimize your IT environment, drive agility and accelerate digital transformation in weeks instead of years. ',
'features-title01' => 'Custom software',
'features-subtitle01' => 'Take your company to another level, supported by a technological platform that meets your needs and requirements.',
'features-title02' => 'Web development',
'features-subtitle02' => 'We create highly robust and scalable corporate websites and platforms using state-of-the-art technology.',
'features-title03' => 'Level II and level III support',
'features-subtitle03' => 'Help desk service, with which you can leverage and improve the response times of your company.',

'features-title04' => 'Hosting administration and support',
'features-subtitle04' => 'We manage your cloud technology infrastructure with the best practices with the importance that the data is yours at all times.',

'features-title05' => 'Mobile application development',
'features-subtitle05' => 'Take your business to the hands of your customers, streamlining processes, and availability of information in the most popular operating systems of the moment Android and iOS.',

'features-title06' => 'SAP Consulting',
'features-subtitle06' => "We carry out integrations with your company's SAP system and we are able to carry out developments both in your ERP and mobile.",

'section-one-title' => 'Digital Transformation',
'section-one-text' => 'Apply technology to the most important processes of your company, evolve the way you work, improve your response times, add value to the customer, manage business risks and discover looking for new income opportunities, thanks to the power that the technological factor can grant you ',

'offers-title' => 'Technologies in which we are experts',
'offers-subtitle' => 'Our team is made up of experts in different technologies including',

'solution-title' => 'Invest smartly',
'solution-text' => 'Use the budget dedicated to the improvement of the technological department of your company in an intelligent way, obtaining quick returns in process improvements and fine-tuning of all the technical services of your company.',

'person1' => 'Andres Garcia',
'phonePerson1' => '+57 320 747 2218',
'cityPerson1' => 'Cali',
'person2' => 'Cesar Villegas',
'phonePerson2' => '+57 323 570 4586',
'cityPerson2' => 'Armenia',
'person3' => 'Juan Orjuela',
'phonePerson3' => '+57 312 360 4331',
'cityPerson3' => 'Bogotá',
'person4' => 'Sebastian Diaz',
'phonePerson4' => '+34 643 14 77 90',
'cityPerson4' => 'Barcelona',

'enter' => 'Enter',
'name' => 'Name',
'email' => 'Email',
'subject' => 'Subject',
'message' => 'Message',
'send' => 'Send',

'language' => 'Language',
'spanish' => 'Spanish',
'english' => 'English',

];

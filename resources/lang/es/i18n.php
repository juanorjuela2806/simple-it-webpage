<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'home' => 'Inicio',
    'contact' => 'Contáctanos',
    'home' => 'Inicio',

    'banner-title' => 'Simple-IT',
    'banner-text' => 'Keep It Simple!',
    'features-title' => 'Servicios',
    'features-text' => 'Utilizando servicios para móviles, Big Data, machine learning, internet de las cosas y más, para ofrecer apps de negocio seguras que integren y optimicen su entorno de TI, impulsen la agilidad y aceleren la transformación digital en semanas en lugar de años.',
    'features-title01' => 'Software a tu medida',
    'features-subtitle01' => 'Lleva tu empresa a otro nivel, soportandose en una plataforma técnologica que cumpla con tus necesidades y requerimientos.',
    'features-title02' => 'Desarrollo web',
    'features-subtitle02' => 'Creamos sitios web y plataformas corporativas técnologicas, muy robustaz y escalables utilizando tecnología de punta.',
    'features-title03' => 'Soporte de nivel II y nivel III',
    'features-subtitle03' => 'Servicio de mesa de ayuda, con el cual puedes apalancar y mejorar los tiempos de respuesta de tu empresa.',

    'features-title04' => 'Administración y soporte de hosting',
    'features-subtitle04' => 'Administramos tu infraestructura técnologica cloud con las mejores prácticas con la importancia de que los datos en todo momento son tuyos.',

    'features-title05' => 'Desarrollo de aplicaciones móviles',
    'features-subtitle05' => 'Lleva tu negocio a las manos de tus clientes, agilizando procesos, y disponibilidad de la información en los sistemas operativos más populares del momento Android e iOS.',

    'features-title06' => 'Consultoría SAP',
    'features-subtitle06' => 'Realizamos integraciones con el sistema SAP de tu compañia y nos encontramos en la capacidad de realizar desarrollos tanto en su ERP como de manera móvil.',

    'section-one-title' => 'Transformación Digital',
    'section-one-text' => 'Aplica tecnología a los procesos más importantes de tu empresa, evoluciona la manera de trabajo, mejora tus tiempos de respuesta, agrega valor al cliente, gestiona riesgos empresariales y descubre busca nuevas oportunidades de ingresos, gracias al poder que te puede otorgar el factor tecnológico',

    'offers-title' => 'Tecnologías en las que somos expertos',
    'offers-subtitle' => 'Nuestro equipo es conformado por expertos en diferentes tecnologías entre ellas',

    'solution-title' => 'Invierte inteligentemente',
    'solution-text' => 'Usa el presupuesto dedicado al mejoramiento del departamento técnologico de tu empresa de una manera inteligente, obteniendo retornos rápidos en mejoras de procesos y puesta a punto de todos los servicios técnologicos de tu empresa.',

    'person1' => 'Andres Garcia',
    'phonePerson1' => '+57 320 747 2218',
    'cityPerson1' => 'Cali',
    'person2' => 'Cesar Villegas',
    'phonePerson2' => '+57 323 570 4586',
    'cityPerson2' => 'Armenia',
    'person3' => 'Juan Orjuela',
    'phonePerson3' => '+57 312 360 4331',
    'cityPerson3' => 'Bogotá',
    'person4' => 'Sebastian Diaz',
    'phonePerson4' => ' +34 643 14 77 90',
    'cityPerson4' => 'Barcelona',

    'enter' => 'Ingrese',
    'name' => 'Nombre',
    'email' => 'Correo',
    'subject' => 'Asunto',
    'message' => 'Mensaje',
    'send' => 'Enviar',

    'language' => 'Idioma',
    'spanish' => 'Español',
    'english' => 'Inglés',
    

    
];

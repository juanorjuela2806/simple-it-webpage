<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::prefix('/store')->group(function(){
    Route::get('/', 'Store\StoreController@getStore');

    Route::get('/products','Store\StoreController@getProducts');
    Route::get('/products/subcategory/{id}','Store\StoreController@getProductsFilteredSubcategory');
    Route::get('/products/category/{id}','Store\StoreController@getProductsFilteredCategory');
    Route::post('products/search','Store\StoreController@getProductsSearch');

    Route::get('/blogs','Store\StoreController@getBlogs');
    Route::get('/blogs/{id}','Store\StoreController@getBlogsSelected');
});

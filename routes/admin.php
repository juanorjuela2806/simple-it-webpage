<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::prefix('/admin')->group(function(){
    Route::get('/', 'Admin\DashboardController@getDashboard');
    Route::get('/users', 'Admin\UserController@getUsers');

    Route::get('/categories', 'Admin\CategoriesController@getCategories');
    Route::get('/categories/{id}/edit', 'Admin\CategoriesController@showCategory');
    Route::get('/categories/{id}/delete', 'Admin\CategoriesController@deleteCategory');
    Route::post('/categories/{id}/edit', 'Admin\CategoriesController@updateCategories');
    Route::post('/categories/add', 'Admin\CategoriesController@postCategory');

    Route::get('/subcategories', 'Admin\SubcategoriesController@getSubcategories');
    Route::get('/subcategories/{id}/delete', 'Admin\SubcategoriesController@deleteSubcategory');
    Route::get('/subcategories/{id}/edit', 'Admin\SubcategoriesController@showSubcategory');
    Route::post('/subcategories/{id}/edit', 'Admin\SubcategoriesController@updateSubategories');
    Route::post('/subcategories/add', 'Admin\SubcategoriesController@postSubcategory');

    Route::get('/subcategories/{id}', 'Admin\SubcategoriesController@getCategory');

    Route::get('/products', 'Admin\ProductController@getProducts');
    Route::get('/products/add', 'Admin\ProductController@addProduct');
    Route::get('/products/{id}/delete', 'Admin\ProductController@deleteProduct');
    Route::get('/products/{id}/edit', 'Admin\ProductController@showProduct');
    Route::post('/products/add','Admin\ProductController@postProduct');
    Route::post('/products/{id}/edit', 'Admin\ProductController@updateProducts');

    //Route::get('/banner', 'Admin\BannerController@getBanner');
    //Route::post('/banner/{id}/edit','Admin\BannerController@editBanner');

    Route::get('/configuration', 'Admin\ConfigurationController@getConfiguration');
    Route::post('/configuration/{id}/edit','Admin\ConfigurationController@editConfiguration');

    Route::get('/sizes', 'Admin\SizesController@getSizes');

    Route::get('/blogs', 'Admin\BlogsController@getBlogs');
    Route::get('/blogs/add', 'Admin\BlogsController@openCreateBlog');
    Route::post('/blogs/add','Admin\BlogsController@postBlog');
    Route::get('/blogs/{id}/edit', 'Admin\BlogsController@showBlog');
    Route::post('/blogs/{id}/edit', 'Admin\BlogsController@updateBlog');
    Route::get('/blogs/{id}/delete', 'Admin\BlogsController@deleteBlog');


    Route::get('/notices', 'Admin\NoticeController@getNotices');
    Route::post('/notices/add','Admin\NoticeController@postNotice');
    Route::get('/notices/{id}/edit', 'Admin\NoticeController@showNotice');
    Route::post('/notices/{id}/edit', 'Admin\NoticeController@updateNotice');
    Route::get('/notices/{id}/delete', 'Admin\NoticeController@deleteNotice');


    Route::get('/acessories', 'Admin\AcessorieController@getAcessories');
    Route::post('/acessories/add','Admin\AcessorieController@postAcessories');
    Route::get('/acessories/{id}/edit', 'Admin\AcessorieController@showAcessories');
    Route::post('/acessories/{id}/edit', 'Admin\AcessorieController@updateAcessories');
    Route::get('/acessories/{id}/delete', 'Admin\AcessorieController@deleteAcessories');
    

});

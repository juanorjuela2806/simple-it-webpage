<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Product extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'products';
    protected $hidden = ['created_at','updated_at'];

    public function join_categories(){
        //hasOne relación uno a uno
        return $this->hasOne(Category::class, 'id','id_category');
    }

    public function join_subcategories(){
        return $this->hasOne(Subcategory::class, 'id','id_subcategory');
    }
}

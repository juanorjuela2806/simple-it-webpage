<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    public function join_categories(){
        //hasOne relación uno a uno
        return $this->hasOne(Category::class, 'id','id_category');
    }
}

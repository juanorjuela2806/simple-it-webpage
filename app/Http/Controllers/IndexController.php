<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App;
use Session;
use Illuminate\Support\Facades\Redirect;

class IndexController extends Controller
{

    public function index(){

        return view('index');

    }

    public function contact(){
        return view('pages.contact');
    }

    public function changeLocale($locale){
        App::setLocale($locale);
        Session::put('locale',$locale);
        return view('index');
    }

    public function goHelpdesk(){
        return Redirect::away("https://google.com");
    }
}

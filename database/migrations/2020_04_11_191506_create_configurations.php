<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigurations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            //Header
            $table->id();
            $table->string('name_store');
            $table->string('title');
            $table->string('subtitle1');
            $table->string('subtitle2');
            $table->string('subtitle3')->nullable();
            $table->string('subtitle4')->nullable();
            $table->string('message1');
            $table->string('message2');
            $table->string('message3')->nullable();
            $table->string('message4')->nullable();
            $table->string('img_banner');
            $table->string('img_path_banner');
            $table->string('img_banner_second');
            $table->string('img_path_banner_second');
            $table->string('phone_wp');
            $table->string('message_wp');
            $table->string('title_footer');
            $table->string('message_footer');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurations');
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Http\Models\Subcategory;

class SubcategorySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){        
        $subcategory = new Subcategory;
        $subcategory->name = 'Subcategoría 1.1';
        $subcategory->id_category = '1';

        $subcategory->save();

        $subcategory = new Subcategory;
        $subcategory->name = 'Subcategoría 1.2';
        $subcategory->id_category = '1';

        $subcategory->save();

        $subcategory = new Subcategory;
        $subcategory->name = 'Subcategoría 2.1';
        $subcategory->id_category = '2';

        $subcategory->save();

        $subcategory = new Subcategory;
        $subcategory->name = 'Subcategoría 2.2';
        $subcategory->id_category = '2';

        $subcategory->save();

        $subcategory = new Subcategory;
        $subcategory->name = 'Subcategoría 3.1';
        $subcategory->id_category = '3';

        $subcategory->save();

        $subcategory = new Subcategory;
        $subcategory->name = 'Subcategoría 3.2';
        $subcategory->id_category = '3';

        $subcategory->save();
    }
}

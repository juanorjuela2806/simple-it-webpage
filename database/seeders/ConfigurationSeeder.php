<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Models\Configuration;

class ConfigurationSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $category = new  Configuration;

        $category->name_store = 'Cero cuatro';
        $category->title = 'Cero cuatro';
        $category->subtitle1 = 'Boutique';
        $category->subtitle2 = 'Subtitulo 2';
        $category->subtitle3 =  'Subtitulo 3';
        $category->subtitle4 =  'Subtitulo 4';
        $category->message1 =  'Mensaje 1';
        $category->message2 =  'Mensaje 2';
        $category->message3 =  'Mensaje 3';
        $category->message4 =  'Mensaje 4';
        $category->img_banner = 'default.jpg';
        $category->img_path_banner =  'default.jpg';
        $category->img_banner_second = 'default.jpg';
        $category->img_path_banner_second = 'default.jpg';
        $category->phone_wp =  '573123251580';
        $category->message_wp =  'Mensaje Whatsapp';
        $category->title_footer =  'Titulo pie página';
        $category->message_footer =  'Texto pie página';

        $category->save();
    }
}

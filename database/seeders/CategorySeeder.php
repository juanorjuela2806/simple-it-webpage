<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Http\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){               
        $category = new Category;
        $category->name = 'Pantalones';
        $category->save();

        $category = new Category;
        $category->name = 'Camisas';
        $category->save();

        $category = new Category;
        $category->name = 'Accesorios';
        $category->save();

        $category = new Category;
        $category->name = 'Chaquetas';
        $category->save();

        $category = new Category;
        $category->name = 'Accesorios';
        $category->save();

        $category = new Category;
        $category->name = 'Vestidos';
        $category->save();

        $category = new Category;
        $category->name = 'Vestidos de baño';
        $category->save();
    }
}

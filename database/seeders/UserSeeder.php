<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->role = 1;
        $user->name = 'Juan';
        $user->lastname = 'Orjuela';
        $user->email = 'juanormo@outlook.com';
        $user->password = Hash::make('nauj0628nauj');

        $user->save();


        $user = new User;
        $user->role = 1;
        $user->name = 'Alejandra';
        $user->lastname = 'Roa';
        $user->email = 'alejandraroabooking@gmail.com';
        $user->password = Hash::make('050413cero');

        $user->save();
    }
}
